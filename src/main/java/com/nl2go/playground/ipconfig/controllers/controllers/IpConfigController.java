package com.nl2go.playground.ipconfig.controllers.controllers;

import com.nl2go.playground.ipconfig.controllers.models.IpConfig;
import com.nl2go.playground.ipconfig.controllers.services.IpConfigMockService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class IpConfigController {

    private IpConfigMockService ipConfigService;

    public IpConfigController(IpConfigMockService ipConfigService) {
        this.ipConfigService = ipConfigService;
    }

    @GetMapping("/ip-configs")
    public Collection<IpConfig> getIpConfigs() {
        return ipConfigService.getIpConfigs();
    }
}
